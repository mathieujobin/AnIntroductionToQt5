#include "jsonfileformat.h"

#include <QFile>
#include <QJsonDocument>
#include <QVariant>
#include <QVariantList>
#include <QVariantMap>

JSONFileFormat::JSONFileFormat(QObject *parent) : QObject(parent)
{

}

bool JSONFileFormat::saveAddressBook(const AddressBook *addressBook, const QString &fileName ) const
{
    // A list of QVariants, holding out address book entries:
    QVariantList entries;

    // Convert each AddressBookEntry to a QVariant:
    for ( auto entry : addressBook->entries() ) {
        // A map of QString to QVariant
        QVariantMap map;
        // Insert the properties of the entry:
        map.insert( "name", entry->name() );
        map.insert( "address", entry->address() );
        map.insert( "birthday", entry->birthday() );
        map.insert( "phoneNumbers", entry->phoneNumbers() );
        // Append the new QVariantMap (which converts to QVariant) to the variant list:
        entries.append( map );
    }

    // Create a new JSON document from QVariant:
    QJsonDocument doc = QJsonDocument::fromVariant( entries );

    // Open a file and write the JSON document to it:
    QFile file( fileName );
    if ( file.open( QIODevice::WriteOnly ) ) {
        file.write( doc.toJson() );
        file.close();
        // Success:
        return true;
    }
    return false;
}

bool JSONFileFormat::loadAddressBook(AddressBook *addressBook, const QString &fileName) const
{
    // Read the JSON from file:
    QFile file( fileName );
    if ( file.open( QIODevice::ReadOnly ) ) {

        // Parse the JSON string:
        QJsonDocument doc = QJsonDocument::fromJson( file.readAll() );

        // If the document consists of an array:
        if ( doc.isArray() ) {
            // Convert the document to a QVariant and this one in turn to a QVariantList:
            QVariantList entries = doc.toVariant().toList();

            // Iterate over all entries:
            for ( auto entryVariant : entries ) {

                // And create a new address book entry from each one:
                auto entry = addressBook->createEntry();
                if ( entry ) {
                    QVariantMap map = entryVariant.toMap();
                    entry->setName( map.value( "name" ).toString() );
                    entry->setAddress( map.value( "address" ).toString() );
                    entry->setBirthday( map.value( "birthday" ).toDate() );
                    entry->setPhoneNumbers( map.value( "phoneNumbers" ).toStringList() );
                }
            }

            // Done, success:
            file.close();
            return true;
        }
        file.close();
    }
    return false;
}

