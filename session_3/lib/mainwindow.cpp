#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QPushButton>
#include <QPlainTextEdit>

MainWindow::MainWindow(AddressBookController *controller, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_addressBookController( controller )
{
    /*
     * We use Q_ASSERT to check the dependencies we received.
     */
    Q_ASSERT( m_addressBookController != nullptr );

    // This sets up the actual UI.
    ui->setupUi(this);

    /*
     * This will connect the signals of the various UI elements to the handlers (slots) we
     * defined. Please note that this must be called after the ui->setupUi() line (before that call,
     * the members of the ui variable are all null).
     */
    setupSignals();

    /*
     * Finally, we can also alter the create UI items a bit, e.g. by setting some custom shortcuts
     * for menu items.
     *
     * Note: Setting shortcuts can also be done in the designer, but sometimes it makes sense
     * to set stuff in C++ instead.
     */
    ui->actionAdd->setShortcut( QKeySequence::New );
    ui->actionEdit->setShortcut( tr( "Ctrl+E" ) );
    ui->actionRemove->setShortcut( QKeySequence::Delete );
}

MainWindow::~MainWindow()
{
    /*
     * The ui variable is not a QObject, hence we must manually delete it.
     */
    delete ui;
}


void MainWindow::setupSignals()
{
    /*
     * This connects various signals in the UI to slots we defined accordingly. Below, we are using
     * the new C++11 method for connections. This new method is "better" compared to the previous
     * syntax because it will cause the compilation to fail if we try to access either signals
     * or slots that do not exist or that are (argument wise) not compatible.
     */
    connect( ui->actionAdd, &QAction::triggered, this, &MainWindow::addEntry );
    connect( ui->actionRemove, &QAction::triggered, this, &MainWindow::removeEntry );
    connect( ui->actionEdit, &QAction::triggered, this, &MainWindow::editItem );
    connect( ui->buttonBox->button( QDialogButtonBox::Save ), &QPushButton::clicked,
             this, &MainWindow::saveItem );
    connect( ui->buttonBox->button( QDialogButtonBox::Discard ), &QPushButton::clicked,
             this, &MainWindow::discardItem );
    connect( ui->buttonBox->button( QDialogButtonBox::Reset ), &QPushButton::clicked,
             this, &MainWindow::resetItem );

    /*
     * For the sake of completeness: This is how the old (pre-c++11) syntax looks like. You
     * should try to avoid this one as much as possible, because the compiler cannot help you
     * checking whether signals/slots exist at all and whether they are compatible. However,
     * sometimes it is necessary or more convenient to use this syntax.
     *
     * Note: The below line should fail, as the slots are not compatible. The thing is you'll have
     * to closely read the debug output of the application, because the failure can only be
     * recognized at run time.
     */
    connect( ui->listWidget, SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)),
             this, SLOT(setWindowTitle(QString)));

    /*
     * In addition to direct signal-slot connections, you can connect a signal also to a
     * (also new in C++11) closure/lambda expression. This is good if the code in the closure
     * is trivial and very short, as with this way we can avoid adding yet another
     * slot to the class (and hence keep the header clean).
     */
    connect( ui->listWidget, &QListWidget::itemDoubleClicked, [this]( QListWidgetItem *item ) {
        ui->listWidget->setCurrentItem( item );
        editItem();
    });
    
    connect( ui->actionOpen, &QAction::triggered, this, &MainWindow::load );
    connect( ui->actionSave, &QAction::triggered, this, &MainWindow::save );

    /*
     * Make sure we update the list widget after loading an address book from file
     * in the background:
     */
    connect( m_addressBookController, &AddressBookController::addressBookLoaded,
             this, &MainWindow::refresh );
}

void MainWindow::renderEntry(AddressBookEntry *entry)
{
    if ( entry ) {
        // We create a new list item and associate it with the address book entry:
        ui->listWidget->addItem( entry->name() );
        auto item = ui->listWidget->item( ui->listWidget->count() - 1 );
        if ( item ) {
            m_entryMap.insert( item, entry );
        }
    }
}

/*
 * The user requested creation of a new address book entry. This handler slot
 * will forward this request to the controller. If the request suceeded, we add the newly created
 * entry to the UI.
 */
void MainWindow::addEntry()
{
    // Only if we're in the list view...
    if ( ui->stackedWidget->currentWidget() == ui->listPage ) {
        // Forward the request to the controller:
        auto entry = m_addressBookController->createEntry();
        // and then render (=add to the list widget) the created entry:
        renderEntry( entry );
    }
}

/*
 * The user requested deletion of an address book entry
 */
void MainWindow::removeEntry()
{
    if ( ui->stackedWidget->currentWidget() == ui->listPage ) {
        // Get the currently selected item:
        auto item = ui->listWidget->currentItem();
        // Look up which entry is associated with it:
        auto entry = m_entryMap.value( item );
        // If both are valid...
        if ( item && entry ) {
            // ...we forward the deletion request to the controller. If this in turn suceeds...
            if ( m_addressBookController->deleteEntry( entry ) ) {
                // We remove the association and delete the UI item:
                m_entryMap.remove( item );
                delete item;
            }
        }
    }
}

/*
 * The user requested to edit an item (and hence show its details)
 */
void MainWindow::editItem()
{
    if ( ui->stackedWidget->currentWidget() == ui->listPage ) {
        // Get the UI item and the associated address book entry:
        auto item = ui->listWidget->currentItem();
        auto entry = m_entryMap.value( item );
        // If both exist...
        if ( item && entry ) {
            // Change the current page to the details page...
            ui->stackedWidget->setCurrentWidget( ui->detailsPage );
            // and call the resetItem() method, which will cause the current item's details to be shown
            resetItem();
        }
    }
}

/*
 * The user requested to save the current item
 */
void MainWindow::saveItem()
{
    // Get the current item and associated address book entry:
    auto item = ui->listWidget->currentItem();
    auto entry = m_entryMap.value( item );
    // If both exist:
    if ( item && entry ) {
        // Copy the current values from the UI into the entry:
        entry->setName( ui->nameEdit->text() );
        entry->setAddress( ui->addressEdit->toPlainText() );
        entry->setBirthday( ui->birthdayEdit->date() );
        // For the phone numbers, let's do some processing:
        QStringList numbers;
        // Split the (plain text) string and iterate over all entries:
        for ( auto number : ui->phoneNumbersEdit->toPlainText().split("\n") ) {
            // If a line is not empty, store it as phone number (without further checks for now...):
            if ( !number.isEmpty() ) {
                numbers.append( number );
            }
        }
        entry->setPhoneNumbers( numbers );

        // Not to forget: We have to update the text shown in the list widget:
        item->setText( entry->name() );
    }
    // Finally, change back to the list page:
    ui->stackedWidget->setCurrentWidget( ui->listPage );
}

/*
 * The user requested to not save the current item.
 */
void MainWindow::discardItem()
{
    // So just return to the list page without further ado:
    ui->stackedWidget->setCurrentWidget( ui->listPage );
}

/*
 * The user requested to reset the UI (and discard any potential changes done on the current item)
 */
void MainWindow::resetItem()
{
    // Get the current UI item and its associated address book entry:
    auto item = ui->listWidget->currentItem();
    auto entry = m_entryMap.value( item );
    // If both exist...
    if ( item && entry ) {
        // Set the UI element's values to the entry's stored values:
        ui->nameEdit->setText( entry->name() );
        ui->addressEdit->setPlainText( entry->address() );
        ui->phoneNumbersEdit->setText( entry->phoneNumbers().join( "\n" ) );
        ui->birthdayEdit->setDate( entry->birthday() );
    }
}

void MainWindow::save()
{
    // Get a file name to save to:
    QString fileName = QFileDialog::getSaveFileName(
                this,
                tr( "Save Address Book" ),
                QString(),
                m_addressBookController->fileNameExtensions() );
    if ( !fileName.isEmpty() ) {
        // Save the current address book:
        m_addressBookController->saveAddressBook( fileName );
    }
}

void MainWindow::load()
{
    // Get a file name to load from:
    QString fileName = QFileDialog::getOpenFileName(
                this,
                tr( "Open Address Book" ),
                QString(),
                m_addressBookController->fileNameExtensions() );
    if ( !fileName.isEmpty() ) {
        // Try to load the file:
        if ( m_addressBookController->loadAddressBook( fileName ) ) {
            refresh();
        }
    }
}

/**
 * @brief Refresh the entries view
 *
 * This will remove all entries currently displayed in the list widget and
 * re-render everything currently stored in the address book belonging to the controller
 * used by the main window.
 */
void MainWindow::refresh()
{
    ui->listWidget->clear();
    m_entryMap.clear();
    for ( auto entry : m_addressBookController->addressBook()->entries() ) {
        renderEntry( entry );
    }
}
