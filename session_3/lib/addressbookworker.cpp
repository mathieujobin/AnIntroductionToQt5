#include "addressbookworker.h"

AddressBookWorker::AddressBookWorker(QObject *parent) : QObject(parent)
{

}

void AddressBookWorker::saveAddressBook(
        const AddressBook *addressBook,
        AddressBookFile *file,
        const QString &fileName )
{
    // Check input:
    Q_ASSERT( addressBook != nullptr );
    Q_ASSERT( file != nullptr );

    // Save the address book (which is a blocking operation):
    bool success = file->saveAddressBook( addressBook, fileName );

    // Emit the signal to let others know we are done:
    emit addressBookSaved( addressBook, success );

    // Delete the file:
    delete file;
}

void AddressBookWorker::loadAddressBook(AddressBook *addressBook, AddressBookFile *file, const QString &fileName)
{
    // Check input:
    Q_ASSERT( addressBook != nullptr );
    Q_ASSERT( file != nullptr );

    // Load the address book (which is a blocking operation):
    bool success = file->loadAddressBook( addressBook, fileName );

    // Let others know we're done
    emit addressBookLoaded( addressBook, success );

    // Delete the file:
    delete file;
}

