#include "addressbookcontroller.h"

#include <QFileInfo>

AddressBookController::AddressBookController(AddressBook *addressBook, PluginManager *pluginManager, QObject *parent) :
    QObject(parent),
    m_addressBook( addressBook ),
    m_pluginManager( pluginManager ),
    m_thread( new QThread( this ) ), // create a background thread
    m_worker( new AddressBookWorker() ) // create a worker to handle requests in the background
{
    /*
     * Via Q_ASSERT we ensure that we got valid pointers. It is good practise to make as much
     * use of these asserts as possible to find issues early in development.
     */
    Q_ASSERT( addressBook != nullptr );
    Q_ASSERT( pluginManager != nullptr );

    Q_ASSERT( m_thread != nullptr );
    Q_ASSERT( m_worker != nullptr );

    // Start the background thread's event loop:
    m_thread->start();

    // Move the worker object to the background thread:
    m_worker->moveToThread( m_thread );

    /*
     * Connect our helper signal startSavingAddressBook() to the
     * AddressBookWorker::saveAddressBook() slot. This connection will be queued, as the controller
     * and worker live in different threads. This means that emitting the
     * startSavingAddressBook() signal will not block the current (GUI) thread.
     */
    connect( this, &AddressBookController::startSavingAddressBook,
             m_worker, &AddressBookWorker::saveAddressBook );

    /*
     * The other direction: After the worker finished saving, we have to clean up. For this,
     * we connect its AddressBookWorker::addressBookSaved() signal to our
     * onAddressBookSaved() slot. This connection as well is queued (i.e. onAddressBookSaved()
     * will be executed in the current (GUI) thread.
     */
    connect( m_worker, &AddressBookWorker::addressBookSaved,
             this, &AddressBookController::onAddressBookSaved );

    /*
     * And the same again for loading:
     */
    connect( this, &AddressBookController::startLoadingAddressBook,
             m_worker, &AddressBookWorker::loadAddressBook );
    connect( m_worker, &AddressBookWorker::addressBookLoaded,
             this, &AddressBookController::onAddressBookLoaded );
}

AddressBookController::~AddressBookController()
{
    // Ask the thread to terminate:
    m_thread->terminate();

    // Wait for 30 seconds  for the thread to terminate:
    m_thread->wait( 30000 );

    // Delete the worker:
    delete m_worker;
}

/*
 * This causes a new address book entry to be created. It just calls the address book's
 * createEntry() method. If that once suceeds and returns a valid entry, we prepare it a bit and
 * return it as it is.
 */
AddressBookEntry* AddressBookController::createEntry()
{
    auto result = m_addressBook->createEntry();
    if ( result ) {
        result->setName( tr( "New entry" ) );
    }
    return result;
}

/*
 * Same here: We just delegate the deletion request to the address book and
 * return the status code.
 */
bool AddressBookController::deleteEntry(AddressBookEntry *entry)
{
    return m_addressBook->deleteEntry( entry );
}

/*
 * This method creates a file name extension string useable with Qt's
 * QFileDialog. It creates the string using the meta information from the
 * loaded plugins.
 */
QString AddressBookController::fileNameExtensions() const
{
    QStringList extensions;
    for ( auto factory : m_pluginManager->fileFactories() ) {
        QString extension = QString( "%1 ( *.%2)" )
                .arg( factory->name() )
                .arg( factory->fileNameExtension() );
        extensions << extension;
    }
    return extensions.join( ";;" );
}

bool AddressBookController::saveAddressBook(const QString &fileName)
{
    // Get the file name extension:
    QFileInfo fi( fileName );
    QString fileNameExtension = fi.suffix();

    // Search for the factory which handles this file extension:
    for ( auto factory : m_pluginManager->fileFactories() ) {
        if ( factory->fileNameExtension() == fileNameExtension ) {

            // Create a file format and use it to safe the current address book:
            auto file = factory->createAddressBookFile();
            if ( file != nullptr ) {
                // Create a new address book...
                AddressBook *copy = new AddressBook();
                if ( copy ) {
                    // ...and copy our entries into it:
                    *copy = *m_addressBook;

                    // Move the new address book into the background thread:
                    copy->moveToThread( m_thread );

                    // Emit our helper signal, this will cause the
                    // AddressBookWorker::saveAddressBook() slot to be started in the
                    // background thread:
                    emit startSavingAddressBook( copy, file, fileName );

                    // Done for now, so return true:
                    return true;
                }
            }
        }
    }
    return false;
}

bool AddressBookController::loadAddressBook(const QString &fileName)
{
    QFileInfo fi( fileName );
    QString fileNameExtension = fi.suffix();

    for ( auto factory : m_pluginManager->fileFactories() ) {
        if ( factory->fileNameExtension() == fileNameExtension ) {
            auto file = factory->createAddressBookFile();
            if ( file != nullptr ) {
                // Create a new address book (acting as container for loaded entries):
                AddressBook *copy = new AddressBook();
                if ( copy ) {
                    // Move the new address book to the background thread:
                    copy->moveToThread( m_thread );

                    // And emit the startLoadingAddressBook() helper signal. This will cause
                    // the AddressBookWorker::loadAddressBook() slot to be executed in the
                    // background:
                    emit startLoadingAddressBook( copy, file, fileName );

                    // Done for now, so return true:
                    return true;
                }
            }
        }
    }
    return false;
}

AddressBook *AddressBookController::addressBook() const
{
    return m_addressBook;
}

void AddressBookController::onAddressBookSaved(const AddressBook *addressBook, bool success)
{
    // The worker assumes no ownershop of the address book, so delete it now (remember:
    // we created a copy of the address book and send it to the background thread):
    if ( addressBook ) {
        delete addressBook;
    }
    Q_UNUSED( success );
}

void AddressBookController::onAddressBookLoaded(AddressBook *addressBook, bool success)
{
    // If loading was successful...
    if ( success && addressBook ) {
        // Copy the entries from the address book used in the background thread into our
        // address book:
        *m_addressBook = *addressBook;

        // And emit the addressBookLoaded() signal to let the view know that it has to
        // update:
        emit addressBookLoaded();
    }
    // The worker assumes no ownershop of the address book, so delete it now (remember:
    // we created a copy of the address book and send it to the background thread):
    if ( addressBook ) {
        delete addressBook;
    }
}

