QT       += core gui widgets

CONFIG += c++11

TARGET = AddressBookLibrary

# Plugins need the implementation of the AddressBook and AddressBookEntry classes.
# Hence, build most files into a shared library against both the main app
# and the plugins can link:
TEMPLATE = lib

DESTDIR = ../bin

SOURCES += \
        mainwindow.cpp \
    addressbookentry.cpp \
    addressbook.cpp \
    addressbookcontroller.cpp \
    pluginmanager.cpp \
    addressbookworker.cpp

HEADERS  += mainwindow.h \
    addressbookentry.h \
    addressbook.h \
    addressbookcontroller.h \
    addressbookfile.h \
    addressbookfilefactory.h \
    pluginmanager.h \
    addressbooklibrary.h \
    addressbookworker.h

FORMS    += mainwindow.ui

# Used to enable exporting of symbols when the library is built
# (required on some platforms/compilers):
DEFINES += ADDRESS_BOOK_LIBRARY
