#ifndef XMLFILEFORMATPLUGIN_H
#define XMLFILEFORMATPLUGIN_H

#include "addressbookfilefactory.h"

#include <QObject>

/**
 * @brief The XMLFileFormatPlugin class
 *
 * An factory which creates XMLFileFormat objects
 */
class XMLFileFormatPlugin : public QObject, public AddressBookFileFactory
{
    Q_OBJECT

    // We implement the AddressBookFileFactory interface
    Q_INTERFACES( AddressBookFileFactory )

    // The unique ID of the plugin
    Q_PLUGIN_METADATA( IID "net.rpdev.AddressBook.XMLFileFormatPlugin" )
public:
    explicit XMLFileFormatPlugin( QObject *parent = 0 );

    // AddressBookFileFactory interface
    AddressBookFile *createAddressBookFile() const override;
    QString name() const override;
    QString fileNameExtension() const override;
};

#endif // XMLFILEFORMATPLUGIN_H
