# An Introduction to Qt 5

This repository holds the source files for an Introduction course to Qt5.
Find out more over at [rpdev.net](http://home.rpdev.net/tutorials/an-introduction-to-qt-5/).



## Content

This repository contains the following projects:



### Session 1: Writing a Widget Based Application

The files for Session 1 are stored in the directory **session_1**. In that
session, a first widget based application is developed (an address book
application) which uses Qt's widgets for the user interface.



### Session 2: Plugins and Various File Formats

The files for Session 2 are stored in the directory **session_2**. This session
gives a short introduction to Qt's plugin mechanism, by showing on how to use it
to implement support for various different file formats in the AddressBook application.

Compared to Session 1, this session restructures the application (i.e. splitting it into
a main application and a library holding the business logic). A plugin manager is
implemented and also three different plugins, adding support for the formats XML, INI and JSON
respectively.



### Session 3: Multi-Threading

The files for Session 3 are stored in the directory **session_3**. In this session, the loading
and saving of address books introduced in Session 2 is moved into a background thread to
avoid blocking the GUI thread. The implementation uses Queued Signal/Slot connections which
completely avoids (manual) locking in the code.
