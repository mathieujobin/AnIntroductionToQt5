#-------------------------------------------------
#
# Project created by QtCreator 2015-09-18T10:55:24
#
#-------------------------------------------------

QT       += core gui

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AddressBook
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    addressbookentry.cpp \
    addressbook.cpp \
    addressbookcontroller.cpp

HEADERS  += mainwindow.h \
    addressbookentry.h \
    addressbook.h \
    addressbookcontroller.h

FORMS    += mainwindow.ui
