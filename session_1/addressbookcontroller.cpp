#include "addressbookcontroller.h"

AddressBookController::AddressBookController(AddressBook *addressBook, QObject *parent) :
    QObject(parent),
    m_addressBook( addressBook )
{
    /*
     * Via Q_ASSERT we ensure that we got valid pointers. It is good practise to make as much
     * use of these asserts as possible to find issues early in development.
     */
    Q_ASSERT( addressBook != nullptr );
}

/*
 * This causes a new address book entry to be created. It just calls the address book's
 * createEntry() method. If that once suceeds and returns a valid entry, we prepare it a bit and
 * return it as it is.
 */
AddressBookEntry* AddressBookController::createEntry()
{
    auto result = m_addressBook->createEntry();
    if ( result ) {
        result->setName( tr( "New entry" ) );
    }
    return result;
}

/*
 * Same here: We just delegate the deletion request to the address book and
 * return the status code.
 */
bool AddressBookController::deleteEntry(AddressBookEntry *entry)
{
    return m_addressBook->deleteEntry( entry );
}

