#include "addressbook.h"

AddressBook::AddressBook(QObject *parent) : QObject(parent)
{

}

AddressBook::Entries AddressBook::entries() const
{
    /*
     * Just return the entry list. If you would use the container classes in the STL, you would
     * rather want to return this as a const reference. However, Qt's containers are
     * implicitly shared and copy-on-write. That means: Creating a copy of a container is
     * negligible in most cases. Only as soon as a modification on the copied container
     * happens, a deep copy is triggered.
     */
    return m_entries;
}

AddressBookEntry *AddressBook::createEntry()
{
    // Try to create a new entry:
    AddressBookEntry *result = new AddressBookEntry( this );
    if ( result ) {
        // If we were able to create a new entry, add it to the list of entries...
        m_entries.append( result );
        // and emit the entryAdded() signal.
        emit entryAdded( result );
    }
    // Finally, return the entry (or nullptr, whatever has is stored in the variable):
    return result;
}

bool AddressBook::deleteEntry(AddressBookEntry *entry)
{
    // Make sure that we own the entry:
    if ( m_entries.contains( entry ) ) {
        /*
         *  If we do, first emit the entryRemoved() signal. Doing it at this place
         * gives connected listeners the chance to access 'entry' while it is still valid.
         */
        emit entryRemoved( entry );
        // Remove all instances of the entry from out list:
        m_entries.removeAll( entry );
        /*
         * Using deleteLater() is an alternative to using delete on an QObject instance.
         * It causes the object to be destroyed as soon as control returns to the applications
         * event loop. On the other side, this means that the caller of the deleteEntry() method
         * can still safely access the entry variable (because it still is valid when we return
         * from this call).
         */
        entry->deleteLater();
        // Deletion succeeded, return false:
        return true;
    }
    // In case we do not own the entry, return false to indicate something's wrong:
    return false;
}

void AddressBook::clear()
{
  while ( !m_entries.isEmpty() ) {
    deleteEntry( m_entries.first() );
  }
}


