#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "addressbookentry.h"
#include "addressbookcontroller.h"
#include "addressbooklibrary.h"

#include <QFileDialog>
#include <QHash>
#include <QListWidgetItem>
#include <QMainWindow>

// Forward declaration:
namespace Ui {
class MainWindow;
}

/**
 * @brief The MainWindow class
 *
 * This class represents our user interface. It pulls in generated code from the *.ui file
 * to show the GUI as designed in Qt Creator/Designer.
 *
 * The MainWindow class is (in terms of the Model/View/Controller pattern) our view part, and hence
 * only minimal work should be done here (ideally only stuff that is directly relevant for
 * showing anything to the user. Additionally, if the user interacts with the UI, this class calls
 * into the controller to delegate trigger any work.
 */
class ADDRESSBOOKLIBRARY_EXPORT MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief The constructor
     *
     * We are using the Dependency Injection (DI) pattern to pass in any dependencies the object
     * requires to work. This helps to keep the code easier to maintain and test,
     * as it breaks up dependecies.
     */
    explicit MainWindow(AddressBookController *controller, QWidget *parent = 0);

    /**
      * @brief Destructor
      *
      * Note that this one is required due to we have to dispose the ui member variable (this one
      * does not derive from QObject and consequentially is not auto destroyed by parent objects
      * as the remaining classes are that we've written.
      */
    ~MainWindow();

private:
    /**
     * @brief This is the actual user interface instance
     */
    Ui::MainWindow *ui;

    /**
     * @brief A lookup map which allows us to find address book entries connected to a given UI item
     */
    QHash<QListWidgetItem*, AddressBookEntry*> m_entryMap;

    /**
     * @brief The address book controller we use
     */
    AddressBookController *m_addressBookController;
    
    /**
     * @brief Sets up signal/slot connections
     *
     * This is a helper method which sets up signal/slot connections. This method gets called in
     * the constructor once when we create the main window.
     */
    void setupSignals();
    
    void renderEntry( AddressBookEntry *entry );

private slots:

    /**
     * @brief Handlers of actions triggered by the user in the UI
     *
     * The following slots are handlers to actions triggered by the user in the graphical user
     * interface. Note that the content of these slots should be kept as minimal as possible
     * and only UI relevant stuff should be done there. Anything else should be
     * moved to the AddressBookController (this will make it easier for us to e.g. completely
     * reimplement the UI but otherwise keep the complete business logic intact).
     */
    void addEntry();
    void removeEntry();
    void editItem();
    void saveItem();
    void discardItem();
    void resetItem();
    void save();
    void load();

};

#endif // MAINWINDOW_H
