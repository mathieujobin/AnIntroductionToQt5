#ifndef ADDRESSBOOKFILE_H
#define ADDRESSBOOKFILE_H

#include "addressbook.h"

#include <QtPlugin>

/**
 * @brief The AddressBookFile class
 *
 * This interface is used to handle save and restore operations on address books. It provides
 * exactly two methods one for reading and one for writing an address book to a given file.
 */
class AddressBookFile {

public:

    virtual ~AddressBookFile() {}

    /**
     * @brief Saves an address book
     *
     * This saves the @p addressBook to the file identified by its @p fileName.
     * Returns true in case the saving succeeded or false otherwise.
     */
    virtual bool saveAddressBook( const AddressBook *addressBook, const QString &fileName ) const = 0;

    /**
     * @brief Loads an address book
     *
     * This loads an @p addressBook from a file identified by its @p fileName. Returns
     * true in case the loading succeeded or false otherwise.
     */
    virtual bool loadAddressBook( AddressBook *addressBook, const QString &fileName ) const = 0;

};

/*
 * Associate the interface with a unique ID
 */
Q_DECLARE_INTERFACE( AddressBookFile, 
                     "net.rpdev.AddressBookFile" )

#endif // ADDRESSBOOKFILE_H

